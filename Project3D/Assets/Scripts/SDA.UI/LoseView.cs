using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SDA.UI
{
    public class LoseView : BaseView
    {
        [SerializeField]
        private Button restartButton;
        
        [SerializeField]
        private TextMeshProUGUI score;

        public override void ShowView()
        {
            base.ShowView();
            restartButton.onClick.AddListener(()=>SceneManager.LoadScene(0));
        }

        public void UpdateScore(int score)
        {
            this.score.text = $"SCORE {score.ToString()}";
        }

    }
}