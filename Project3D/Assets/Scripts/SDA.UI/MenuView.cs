using System.Collections;
using TMPro;
using UnityEngine;

namespace SDA.UI
{
    public class MenuView : BaseView
    {
        [SerializeField]
        private TextMeshProUGUI bestScore;

        [SerializeField]
        private TextMeshProUGUI lastScore;

        [SerializeField]
        private TextMeshProUGUI pressAnyKey;

        private Coroutine currentCor;
        public void UpdateScores(int bestScore, int lastScore)
        {
            this.bestScore.text = bestScore.ToString();
            this.lastScore.text = lastScore.ToString();
        }

        public override void ShowView()
        {
            base.ShowView();
            currentCor = StartCoroutine(AnimateText());
        }

        public override void HideView()
        {
            base.HideView();
            StopCoroutine(currentCor);
        }

        private IEnumerator AnimateText()
        {
            while (true)
            {
                var randomLetter = Random.Range(32, 127);
                char c = (char) randomLetter;
                pressAnyKey.text = $"PRESS {c.ToString()} KEY TO START!";
                yield return new WaitForSeconds(.5f);
            }
        }
    }
}