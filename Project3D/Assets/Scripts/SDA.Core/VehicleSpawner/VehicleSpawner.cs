using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Core
{
    public class VehicleSpawner : MonoBehaviour
    {
        [SerializeField]
        private NormalVehiclePool normalVehiclePool;

        private List<ISpawnable> lanesInGame = new List<ISpawnable>();
        private Dictionary<ISpawnable, Coroutine> laneToCoroutineDic = new Dictionary<ISpawnable, Coroutine>();

        public void InitSpawner()
        {
            normalVehiclePool.Init(10);
        }
        
        private IEnumerator SpawnVehiclesOnLane(ISpawnable lane)
        {
            while (true)
            {
                var vehicle = normalVehiclePool.GetFromPool(lane.GetSpawnPointPosition());
                vehicle.AddListener(DespawnVehicle);
                vehicle.transform.rotation = lane.GetSpawnPointRotation();
                vehicle.StartMovement();
                
                var timeFrame = lane.GetTimeFrame();
                var timeToNextSpawn = Random.Range(timeFrame.x, timeFrame.y);
                yield return new WaitForSeconds(timeToNextSpawn);
            }
        }

        private void DespawnVehicle(BaseVehicle vehicle)
        {
            normalVehiclePool.ReturnToPool(vehicle as NormalVehicle);
        }

        public void Subscribe(ISpawnable item)
        {
            if (!lanesInGame.Contains(item))
                lanesInGame.Add(item);

            if (!laneToCoroutineDic.ContainsKey(item))
            {
                var coroutine = StartCoroutine(SpawnVehiclesOnLane(item));
                laneToCoroutineDic.Add(item, coroutine);
            }
        }

        public void Unsubscribe(ISpawnable item)
        {
            if (lanesInGame.Contains(item))
            {
                if (laneToCoroutineDic.ContainsKey(item))
                {
                    StopCoroutine(laneToCoroutineDic[item]);
                    laneToCoroutineDic.Remove(item);
                }

                lanesInGame.Remove(item);
            }
        }
    }
}