using UnityEngine;

namespace SDA.Core
{
    public interface ISpawnable
    {
        Vector3 GetSpawnPointPosition();
        Quaternion GetSpawnPointRotation();
        Vector2 GetTimeFrame();
    }
}