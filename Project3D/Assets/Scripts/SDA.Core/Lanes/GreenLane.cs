using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Core
{
    public class GreenLane : BaseLane
    {
        [SerializeField]
        private GameObject[] obstacles;

        [SerializeField]
        private Transform[] spots;

        private List<GameObject> spawnedObj = new List<GameObject>();

        public override void PrepareForDeactivate()
        {
            foreach (var val in spawnedObj)
            {
                Destroy(val.gameObject);
            }
            
            spawnedObj.Clear();
        }

        public override void RefreshObjectState()
        {
            var usedSpots = new List<int>();
            for (int i = 0; i < obstaclesLimit; ++i)
            {
                var spotIdx = -1;
                do
                {
                    spotIdx = Random.Range(0, spots.Length);

                } while (usedSpots.Contains(spotIdx));
                usedSpots.Add(spotIdx);
                var obstacleObj = Instantiate(obstacles[Random.Range(0, obstacles.Length)],
                    spots[spotIdx].transform.position, Quaternion.identity, this.transform);

                spawnedObj.Add(obstacleObj);
            }
        }
    }
}