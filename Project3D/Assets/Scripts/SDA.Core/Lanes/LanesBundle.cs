using System;
using UnityEngine;

namespace SDA.Core
{
    public enum LaneType
    {
        Green,
        Road,
        Track
    }

    public enum  LaneDirection
    {
        Left, //0
        Right //1
    }

    [Serializable]
    public class LaneData
    {
        public LaneType type;
        public LaneDirection dir;
        public bool enableAdditionalObjects;
        public int obstaclesLimit;
    }

    [CreateAssetMenu(menuName = "Bundle/new bundle")]
    public class LanesBundle : ScriptableObject
    {
        public LaneData[] lanes;
    }
}