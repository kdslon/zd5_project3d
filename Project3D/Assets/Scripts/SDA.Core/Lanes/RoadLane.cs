using System;
using UnityEngine;

namespace SDA.Core
{
    public class RoadLane : BaseLane, ISpawnable
    {
        [SerializeField]
        private GameObject[] whiteRectangles;

        public override void PrepareForActivate()
        {
            foreach(var rect in whiteRectangles)
                rect.SetActive(false);
        }

        public override void RefreshObjectState()
        {
            foreach(var rect in whiteRectangles)
                rect.SetActive(enableAdditionalObjects);
        }
    }

}