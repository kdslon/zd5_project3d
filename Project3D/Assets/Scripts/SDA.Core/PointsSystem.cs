using UnityEngine;

namespace SDA.Core
{
    public class PointsSystem : MonoBehaviour
    {
        private int bestScore;
        public int currentScore;

        public int BestScore
        {
            get
            {
                if (currentScore < bestScore)
                    return bestScore;
                return currentScore;
            }
        }

        public void Init(int bestScore)
        {
            this.bestScore = bestScore;
        }

        public void AddPoint()
        {
            currentScore++;
        }
    }
}