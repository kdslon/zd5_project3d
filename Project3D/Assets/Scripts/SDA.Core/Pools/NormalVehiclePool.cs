using System.Collections;
using System.Collections.Generic;
using SDA.Architecture;
using UnityEngine;

namespace SDA.Core
{
    public class NormalVehiclePool : Pool<NormalVehicle> { }
}