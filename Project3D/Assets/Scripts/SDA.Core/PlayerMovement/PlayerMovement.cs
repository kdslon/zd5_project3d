using System;
using DG.Tweening;
using UnityEngine;

namespace SDA.Core
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private Collider collider;
        
        private bool canMove = true;
        private float highestZ;

        public Action OnLose;
        
        public void UpdateMovement(Vector2 input, PointsSystem pointsSystem)
        {
            if(input.x > 0)
                MoveRight();
            else if(input.x < 0)
                MoveLeft();
            else if(input.y > 0)
                MoveForward(pointsSystem);
            else if(input.y < 0)
                MoveBack();
        }

        private void MoveForward(PointsSystem pointsSystem)
        {
            if (!canMove)
                return;

            canMove = false;
            transform.DORotate(new Vector3(0, 0, 0), .2f);
            var endValue = transform.position + Vector3.forward * 10f;
            if (endValue.z > highestZ)
            {
                this.highestZ = endValue.z;
                pointsSystem.AddPoint();
                Debug.Log(pointsSystem.currentScore.ToString());
            }
            
            transform.DOJump(endValue, 5f, 1, .2f).OnComplete(EnableMovement);
        }

        private void EnableMovement()
        {
            canMove = true;
        }

        private void MoveBack()
        {
            if (!canMove)
                return;

            canMove = false;
            transform.DORotate(new Vector3(0, -180, 0), .2f);
            var endValue = transform.position - Vector3.forward * 10f;

            transform.DOJump(endValue, 5f, 1, .2f).OnComplete(EnableMovement);
        }

        private void MoveLeft()
        {
            if (!canMove)
                return;

            canMove = false;
            transform.DORotate(new Vector3(0, -90, 0), .2f);
            var endValue = transform.position - Vector3.right * 10f;

            transform.DOJump(endValue, 5f, 1, .2f).OnComplete(EnableMovement);
        }

        private void MoveRight()
        {
            if (!canMove)
                return;

            canMove = false;
            transform.DORotate(new Vector3(0, 90, 0), .2f);
            var endValue = transform.position + Vector3.right * 10f;

            transform.DOJump(endValue, 5f, 1, .2f).OnComplete(EnableMovement);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.CompareTag("Vehicle"))
            {
                this.collider.enabled = false;
                OnLose.Invoke();
            }

        }
    }
}
