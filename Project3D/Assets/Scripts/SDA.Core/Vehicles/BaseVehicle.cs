using System;
using SDA.Architecture;
using UnityEngine;

namespace SDA.Core
{
    public abstract class BaseVehicle : MonoBehaviour, IPoolable
    {
        private Action<BaseVehicle> onDespawn;
        public virtual void PrepareForActivate()
        {
            
        }

        public virtual void PrepareForDeactivate()
        {
            onDespawn = null;
        }

        public void AddListener(Action<BaseVehicle> callback)
        {
            onDespawn += callback;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Despawn"))
            {
                onDespawn?.Invoke(this);
            }
        }
    }
}