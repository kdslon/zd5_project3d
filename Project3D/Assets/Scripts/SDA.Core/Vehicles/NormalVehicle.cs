using UnityEngine;

namespace SDA.Core
{
    public class NormalVehicle : BaseVehicle
    {
        [SerializeField]
        private Rigidbody vehicleRb;

        [SerializeField]
        private float speed;

        public void StartMovement()
        {
            vehicleRb.AddForce(this.transform.forward * speed, ForceMode.Impulse);
        }

        public override void PrepareForActivate()
        {
            base.PrepareForActivate();
            vehicleRb.velocity = Vector3.zero;
        }
    }
}