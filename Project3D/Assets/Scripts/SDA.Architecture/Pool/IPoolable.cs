namespace SDA.Architecture
{
    public interface IPoolable
    {
        public void PrepareForActivate();
        public void PrepareForDeactivate();
    }
}