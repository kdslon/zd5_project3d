using SDA.Core;
using SDA.Data;
using SDA.UI;
using UnityEngine;

namespace SDA.Architecture
{
    public class GameController : MonoBehaviour
    {
        private BaseState currentState;
        
        private SaveSystem saveSystem;
        public SaveSystem SaveSystem => saveSystem;

        [SerializeField]
        private CrossyInput crossyInput;
        public CrossyInput CrossyInput => crossyInput;
        
        [SerializeField]
        private MenuView menuView;
        public MenuView MenuView => menuView;
        
        [SerializeField]
        private LoseView loseView;
        public LoseView LoseView => loseView;

        [SerializeField]
        private LevelSpawner levelSpawner;
        public LevelSpawner LevelSpawner => levelSpawner;

        [SerializeField]
        private VehicleSpawner vehicleSpawner;
        public VehicleSpawner VehicleSpawner => vehicleSpawner;

        [SerializeField]
        private CameraMovementController cameraMovementController;
        public CameraMovementController CameraMovementController => cameraMovementController;
        
        [SerializeField]
        private PlayerMovement playerMovement;
        public PlayerMovement PlayerMovement => playerMovement;
        
        [SerializeField]
        private PointsSystem pointsSystem;
        public PointsSystem PointsSystem => pointsSystem;
        public void Start()
        {
            saveSystem = new SaveSystem();
            saveSystem.LoadData();
            
            ChangeState(new MenuState());
        }

        public void Update()
        {
            currentState?.UpdateState();
        }

        public void OnDestroy()
        {
            saveSystem.SaveData();
        }

        public void ChangeState(BaseState newState)
        {
            currentState?.DestroyState(); //if(currentState != null) => currentState?.Method()
            currentState = newState;
            currentState?.InitState(this);
        }
    }
}