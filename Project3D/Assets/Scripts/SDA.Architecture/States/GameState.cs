using SDA.Core;

namespace SDA.Architecture
{
    public class GameState : BaseState
    {
        public override void InitState(GameController controller)
        {
            base.InitState(controller);
            controller.CrossyInput.IsMovedPressed += 
                (input)=>controller.PlayerMovement.UpdateMovement(input,controller.PointsSystem);
            base.controller.PlayerMovement.OnLose += () => controller.ChangeState(new LoseState());
        }

        public override void UpdateState()
        {
            controller.CameraMovementController.UpdateCameraPosition();
        }

        public override void DestroyState()
        {
            controller.CrossyInput.ClearAllInputs();
        }
    }
}
