namespace SDA.Architecture
{
    public class MenuState : BaseState
    {
        public override void InitState(GameController controller)
        {
            base.InitState(controller);
            controller.MenuView.ShowView();
            controller.CrossyInput.IsStartPressed += StartGame;
            controller.MenuView.UpdateScores(controller.SaveSystem.PlayerData.bestScore, 
                controller.SaveSystem.PlayerData.lastScore);
            controller.PointsSystem.Init(controller.SaveSystem.PlayerData.bestScore);
            controller.VehicleSpawner.InitSpawner();
            controller.LevelSpawner.Init(2);
        }

        public override void UpdateState()
        {
            
        }

        public override void DestroyState()
        {
            controller.MenuView.HideView();
            controller.CrossyInput.ClearAllInputs();
        }

        private void StartGame(bool isPressed)
        {
            if(isPressed)
                controller.ChangeState(new GameState());
        }
    }
}