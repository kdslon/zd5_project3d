namespace SDA.Architecture
{
    public abstract class BaseState
    {
        protected GameController controller;
        public virtual void InitState(GameController controller)
        {
            this.controller = controller;
        }

        public virtual void UpdateState()
        {
        }

        public virtual void DestroyState()
        {
            
        }
    }
}
