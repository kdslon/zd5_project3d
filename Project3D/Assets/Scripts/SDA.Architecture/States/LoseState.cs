using SDA.Architecture;

namespace SDA.Core
{
    public class LoseState : BaseState
    {
        public override void InitState(GameController controller)
        {
            base.InitState(controller);
            base.controller.LoseView.ShowView();
            base.controller.SaveSystem.PlayerData.bestScore = controller.PointsSystem.BestScore;
            base.controller.SaveSystem.SaveData();
            base.controller.LoseView.UpdateScore(controller.PointsSystem.currentScore);
        }

        public override void UpdateState()
        {
            
        }

        public override void DestroyState()
        {
            controller.LoseView.HideView();
        }
    }
}